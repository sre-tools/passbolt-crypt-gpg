#
# Copyright (c) 2018 Make.org
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permis ''sions and
# limitations under the License.
#

require 'spec_helper'
require 'pp'

user = 'www-data'
cmd = '/var/www/passbolt/bin/cake passbolt healthcheck gpg'
output = `su -s /bin/bash -c 'source /etc/environment && #{cmd}' #{user}`

data = output.lines.drop(12)
             .map { |l| l.chomp.strip }
             .delete_if { |l| l.eql?("\n") || l.start_with?('[HELP]') }

all_tests = {}
data.each do |line|
  if (match = line.match(/\[(WARN|PASS|FAIL)\] (.+)/))
    result, msg = match.captures
    all_tests[all_tests.keys.last] << { msg => result }
  else
    all_tests[line] = []
  end
end

all_tests.each_pair do |context, tests|
  describe context do
    tests.each do |test|
      test.each_pair do |description, result|
        it description do
          expect(result).to eq('PASS').or eq('WARN')
        end
      end
    end
  end
end
