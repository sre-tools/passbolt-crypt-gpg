# frozen_string_literal: true

# Copyright (c) 2018 Make.org
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

# Install needed packages
package %w[git patch]

root_dir = '/root'
patch = 'crypt-gpg.patch'
install_dir = '/var/www/passbolt'

remote_file '/usr/local/bin/composer' do
  source 'https://getcomposer.org/download/1.6.5/composer.phar'
  owner 'root'
  group 'staff'
  mode '0755'
  action :create
end

cookbook_file "#{root_dir}/#{patch}" do
  source patch
  notifies :run, 'execute[patch passbolt]', :immediate
end

execute 'patch passbolt' do
  command "patch -d #{install_dir} --strip 1 < #{root_dir}/#{patch}"
  notifies :run, 'execute[launch composer install]', :immediate
  action :nothing
end

execute 'launch composer install' do
  command "composer update --no-dev -d #{install_dir} pear/crypt_gpg"
  action :nothing
end
