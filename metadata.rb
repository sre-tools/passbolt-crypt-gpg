name 'passbolt-test'
maintainer 'Make.org'
maintainer_email 'sre@make.org'
license 'Apache-2.0'
description 'Helper cookbook to test Passbolt with Crypt-GPG'
long_description 'Helper cookbook to test Passbolt with Crypt-GPG'
source_url 'https://gitlab.com/sre-tools/passbolt'
issues_url 'https://gitlab.com/sre-tools/passbolt/issues'
version '1.0.0'

chef_version '>= 13.0'

supports 'centos', '>= 7.4'
