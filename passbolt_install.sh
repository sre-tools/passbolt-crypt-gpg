#!/bin/bash
#
# Copyright (c) 2018 Make.org
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# Performs Passbolt installation or update with Crypt_GPG patch

set -e

GIT_URL='https://github.com/passbolt/passbolt_api'

usage() {
  echo "Usage: ${0} -d <install_dir> [-p <patch_file>] -m [install|update]"
  exit 1
}

error() {
  echo "${1}"
  exit 1
}

[ $# -eq 0 ] && usage

while getopts ':d:p:' opt; do
  case "${opt}" in
    d) INSTALL_DIR=${OPTARG} ;;
    p) PATCH=${OPTARG} ;;
    *) usage ;;
  esac
done

shift $((OPTIND-1))
[ "${1:-}" = "--" ] && shift
MODE="${@}"

if [ -z "${INSTALL_DIR}" ]; then
  usage
fi

if [[ ! "${MODE}" =~ ^(install|update)$ ]]; then
  error "Please specify mode install/update as last parameter"
fi

if [ "$MODE" = "install" ] && [ ! -f "${PATCH}" ]; then
  error "Patch is not specified or not a file"
fi

if [ "$MODE" = "install" ]; then
  git clone "${GIT_URL}" "${INSTALL_DIR}"
  git -C ${INSTALL_DIR} checkout \
    $(git -C ${INSTALL_DIR} describe origin --abbrev=0)
  patch -d "${INSTALL_DIR}" --strip 1 < "${PATCH}"
elif [ "$MODE" = "update" ]; then
  git -C ${INSTALL_DIR} stash
  git -C ${INSTALL_DIR} checkout \
    $(git -C ${INSTALL_DIR} describe origin --abbrev=0)
  git -C ${INSTALL_DIR} stash pop
fi
